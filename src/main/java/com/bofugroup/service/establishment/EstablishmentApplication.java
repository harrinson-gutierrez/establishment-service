package com.bofugroup.service.establishment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class EstablishmentApplication{

	public static void main(String[] args) {
		SpringApplication.run(EstablishmentApplication.class, args);
	}
	
}
